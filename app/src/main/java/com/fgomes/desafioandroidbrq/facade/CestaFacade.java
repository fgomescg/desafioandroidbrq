package com.fgomes.desafioandroidbrq.facade;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.fgomes.desafioandroidbrq.activitys.CarDetailActivity;
import com.fgomes.desafioandroidbrq.activitys.CestaActivity_;
import com.fgomes.desafioandroidbrq.entity.Car;
import com.fgomes.desafioandroidbrq.entity.Cesta;
import com.fgomes.desafioandroidbrq.util.Utility;

import java.util.ArrayList;
import java.util.List;


public class CestaFacade {

    public void InserirItemCesta(Context context, Car car)
    {
        Utility Util = new Utility();
        Cesta cesta = new Cesta();
        List<Car> listCarrosCesta;
        List<Car> listCarrosCestaModificada = new ArrayList<>();
        try {

            //Lendo a cesta de Carros
            if(Util.LerObjeto(context, "cesta.bin") != null)
                cesta = (Cesta) Util.LerObjeto(context, "cesta.bin");

            if(cesta.getcarList() == null) {
                cesta = new Cesta();
                listCarrosCesta = new ArrayList<>();
                listCarrosCesta.add(car);
                cesta.setcarList(listCarrosCesta);
            } else {
                listCarrosCesta = cesta.getcarList();
                //verifica se já existe o carro na lista,
                // atualiza o objeto, pois pode vir dos detalhes e vir com qtd.

                for (Car carro : listCarrosCesta) {
                    //Se o carro já existe na cesta, adiciona na cesta modificada.
                    if (carro.getId().equals(car.getId())) {
                        listCarrosCestaModificada.add(carro);
                    }
                }
                //remove e adiciona novamente para atualizar a qtd
                listCarrosCesta.removeAll(listCarrosCestaModificada);
                listCarrosCesta.add(car);

                //Setando a lista atualizada na cesta
                cesta.setcarList(listCarrosCesta);
            }
            //Persistindo a cesta.
            Util.GravaObjeto(context, "cesta.bin", cesta);

            Toast.makeText(context, car.getNome() + " adicionado com sucesso!", Toast.LENGTH_LONG).show();

            Intent intent = new Intent(context, CestaActivity_.class);
            context.startActivity(intent);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void RemoverItemCesta(Context context, Car car)
    {
        Utility Util = new Utility();
        Cesta cesta = new Cesta();
        List<Car> listCarrosCesta;
        List<Car> listCarrosCestaModificada = new ArrayList<>();

        try {

            //Lendo a cesta de Carros
            if(Util.LerObjeto(context, "cesta.bin") != null)
                cesta = (Cesta) Util.LerObjeto(context, "cesta.bin");

            listCarrosCesta = cesta.getcarList();
            //verifica se já existe o carro na lista, e remove

            for (Car carro : listCarrosCesta) {
                //Se o carro já existe na cesta, adiciona na cesta modificada.
                if (carro.getId().equals(car.getId())) {
                    listCarrosCestaModificada.add(carro);
                }
            }
            //remove
            listCarrosCesta.removeAll(listCarrosCestaModificada);

            //Setando a lista atualizada na cesta
            cesta.setcarList(listCarrosCesta);

            //Persistindo a cesta.
            Util.GravaObjeto(context, "cesta.bin", cesta);

            Toast.makeText(context, car.getNome() + " removido com sucesso!", Toast.LENGTH_LONG).show();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
