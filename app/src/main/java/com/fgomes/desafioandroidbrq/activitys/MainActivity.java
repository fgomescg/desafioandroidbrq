package com.fgomes.desafioandroidbrq.activitys;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import com.fgomes.desafioandroidbrq.R;
import com.fgomes.desafioandroidbrq.adapters.NewAdapter;
import com.fgomes.desafioandroidbrq.api.RestClient;
import com.fgomes.desafioandroidbrq.api.iCarService;
import com.fgomes.desafioandroidbrq.entity.Car;

import com.fgomes.desafioandroidbrq.interfaces.RecyclerViewOnClickListener;
import com.fgomes.desafioandroidbrq.util.Utility;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity  {


    @ViewById(R.id.tool_bar)
    Toolbar mToolbar;
    @ViewById(R.id.rv_list)
    RecyclerView mRecyclerView;
    @ViewById(R.id.fab)
    FloatingActionMenu fab;
    @ViewById(R.id.fab1)
    FloatingActionButton fab1;
    @ViewById(R.id.fab2)
    FloatingActionButton fab2;
    @ViewById(R.id.fab3)
    FloatingActionButton fab3;
    @ViewById(R.id.progressbar)
    ProgressBar progressBar;

    protected List<Car> mListCar;

    @AfterViews
    protected void init() {

        mToolbar.setTitle(getString(R.string.car_list_title));
        setSupportActionBar(mToolbar);

        fab.setOnMenuToggleListener(new FloatingActionMenu.OnMenuToggleListener() {
            @Override
            public void onMenuToggle(boolean b) {
                //Toast.makeText(this, "Is menu opened? " + (b ? "true" : "false"), Toast.LENGTH_SHORT).show();
            }
        });
        fab.showMenuButton(true);
        fab.setClosedOnTouchOutside(true);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0) {
                    fab.hideMenuButton(true);
                } else {
                    fab.showMenuButton(true);
                }

            }
        });

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(llm);

        getCars();
    }


    protected void getCars() {
        progressBar.setVisibility(View.VISIBLE);

        iCarService serviceAPI = RestClient.CarService();

        Call<List<Car>> loadCars = serviceAPI.getCars();

        loadCars.enqueue(new Callback<List<Car>>() {
            @Override
            public void onResponse(Call<List<Car>> call, Response<List<Car>> response) {
                mListCar = response.body();
                NewAdapter Adapter = new NewAdapter(MainActivity.this, mListCar);
                mRecyclerView.setAdapter(Adapter);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<List<Car>> call, Throwable t) {
                Log.i("getShots Error", t.getMessage());
                Toast.makeText(MainActivity.this, R.string.erro_sem_conexao, Toast.LENGTH_LONG).show();
            }
        });

    }


    @Click({R.id.fab1,R.id.fab2,R.id.fab3})
    void socialClick(View v) {
        String url = "";
        switch( v.getId() ){
            case R.id.fab1:
                url = "https://www.facebook.com/fabio.gomes.cg";
                break;
            case R.id.fab2:
                url = "https://plus.google.com/+FabioGomesCG";
                break;
            case R.id.fab3:
                url = "https://www.linkedin.com/in/fgomescg";
                break;
        }

        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);

    }



    @Override
    public void onBackPressed(){

        if(fab.isOpened()) {
            fab.close(true);
        }
        else {
            new AlertDialog.Builder(this)
                    .setTitle(R.string.titulo_sair)
                    .setMessage(R.string.saida_confirmacao)
                    .setNegativeButton(R.string.nao, null)
                    .setPositiveButton(R.string.sim, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface arg0, int arg1) {
                            //MainActivity.super.onBackPressed();
                            finish();
                        }
                    }).create().show();
        }
    }
}
