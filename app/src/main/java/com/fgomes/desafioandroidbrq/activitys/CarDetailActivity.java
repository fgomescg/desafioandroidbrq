package com.fgomes.desafioandroidbrq.activitys;


import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.fgomes.desafioandroidbrq.R;
import com.fgomes.desafioandroidbrq.api.RestClient;
import com.fgomes.desafioandroidbrq.api.iCarService;
import com.fgomes.desafioandroidbrq.entity.*;
import com.fgomes.desafioandroidbrq.facade.CestaFacade;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.fgomes.desafioandroidbrq.util.Utility;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@EActivity(R.layout.activity_car_detail)
public class CarDetailActivity extends AppCompatActivity {

    public Car carDetail;
    public int carroId;
    private Utility Util = new Utility();
    CestaFacade facade = new CestaFacade();

    @ViewById(R.id.tool_bar)
    Toolbar mToolbar;
    @ViewById(R.id.media_image)
    ImageView ivShot;
    @ViewById(R.id.primary_text)
    TextView tvTitle;
    @ViewById(R.id.tv_description)
    TextView tvDescription;
    @ViewById(R.id.price_text)
    TextView txtPrice;
    @ViewById(R.id.spinnerQtd)
    Spinner spinnerQtd;
    @ViewById(R.id.btAdicionar)
    Button btAdicionar;
    @ViewById(R.id.detalhe_text)
    TextView detalhe_text;

    @AfterViews
    protected void init() {

        mToolbar.setNavigationIcon(R.mipmap.ic_up21);
        mToolbar.setTitle(getString(R.string.car_detail_title));
        setSupportActionBar(mToolbar);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        carroId = (int) getIntent().getSerializableExtra("carroId");

        //chamada do serviço
        iCarService serviceAPI = RestClient.CarService();
        Call<Car> loadCar = serviceAPI.getCar(carroId);

        loadCar.enqueue(new Callback<Car>() {
            @Override
            public void onResponse(Call<Car> call, Response<Car> response) {
                carDetail = response.body();
                //Persistindo o carro selecionado
                Util.GravaObjeto(CarDetailActivity.this, "carro.bin", carDetail);
                //Imagem
                Picasso.with(CarDetailActivity.this).load(carDetail.getImagem())
                        .placeholder(R.drawable.loagind_image)
                        .error(R.drawable.loading_error).into(ivShot);

                tvTitle.setText(carDetail.getNome());

                if(carDetail.getDescricao() != null) {
                    tvDescription.setText(Html.fromHtml(carDetail.getDescricao()));
                    tvDescription.setMovementMethod(LinkMovementMethod.getInstance());                }
                    NumberFormat nf = new DecimalFormat("R$ #,##0.00",
                            new DecimalFormatSymbols(new Locale("pt", "BR")));
                    txtPrice.setText(nf.format(carDetail.getPreco()));

                List<String> lstQtd = new ArrayList<>();
                    //populando o spinner
                for(int i = 1; i < carDetail.getQuantidade()+1; i++)
                {
                    lstQtd.add(String.valueOf(i));
                }

                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(CarDetailActivity.this,
                        android.R.layout.simple_spinner_item, lstQtd);

                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerQtd.setAdapter(dataAdapter);
            }

            @Override
            public void onFailure(Call<Car> call, Throwable t) {
                Log.i("getCar Error", t.getMessage());
                Toast.makeText(CarDetailActivity.this, R.string.erro_sem_conexao, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Click(R.id.btAdicionar)
    void addCesta()
    {
        try {
            //Lendo o carro atual
            Car carroDetalhe = (Car) Util.LerObjeto(CarDetailActivity.this, "carro.bin");
            //setando a qtd selecionada no spinner
            carroDetalhe.setQuantidade(Integer.parseInt(spinnerQtd.getSelectedItem().toString()));

            facade.InserirItemCesta(CarDetailActivity.this, carroDetalhe);

            Toast.makeText(CarDetailActivity.this, carroDetalhe.getNome() + " adicionado com sucesso!", Toast.LENGTH_LONG).show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
