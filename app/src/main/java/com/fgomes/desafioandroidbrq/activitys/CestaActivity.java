package com.fgomes.desafioandroidbrq.activitys;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.fgomes.desafioandroidbrq.R;
import com.fgomes.desafioandroidbrq.adapters.NewAdapterCesta;
import com.fgomes.desafioandroidbrq.database.DataBaseController;
import com.fgomes.desafioandroidbrq.entity.Cesta;
import com.fgomes.desafioandroidbrq.util.Utility;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import static android.os.Build.MODEL;

@EActivity(R.layout.activity_cesta)
public class CestaActivity extends AppCompatActivity  {

    @ViewById(R.id.tool_bar)
    Toolbar mToolbar;
    @ViewById(R.id.rv_list_cesta)
    RecyclerView mRecyclerView;
    @ViewById(R.id.progressbar)
    ProgressBar progressBar;

    private Utility Util = new Utility();

    @AfterViews
    protected void init() {

        mToolbar.setNavigationIcon(R.mipmap.ic_up21);
        mToolbar.setTitle(getString(R.string.car_cesta_title));
        setSupportActionBar(mToolbar);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mRecyclerView.setHasFixedSize(true);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(llm);

        MontarCesta();
    }


    protected void MontarCesta() {

        progressBar.setVisibility(View.INVISIBLE);

        //Lendo a cesta de Carros
        Cesta cesta = (Cesta) Util.LerObjeto(this, "cesta.bin");

        //Se a lista ficar vazia, fecha a activity.
        if(cesta.getcarList().size() <= 0){
            Toast.makeText(this, R.string.cesta_vazia, Toast.LENGTH_LONG).show();
            finish();
        }

        NewAdapterCesta Adapter = new NewAdapterCesta(this, cesta.getcarList());
        mRecyclerView.setAdapter(Adapter);
    }

    @Click(R.id.btFinalizaCompra)
    void FinalizaCompra() {

        double limite = 100000.0;
        double valor_total_cesta = 0;
        Cesta cesta;

        try
        {
        //Lendo a cesta de Carros
        if (Util.LerObjeto(this, "cesta.bin") != null){
            cesta = (Cesta) Util.LerObjeto(this, "cesta.bin");
            valor_total_cesta = cesta.getValorTotalCesta();
        }

        //Validando o valor máximo da compra
        if(valor_total_cesta > limite) {
            Toast.makeText(this, R.string.Limite_Compra, Toast.LENGTH_LONG).show();
        }
        else
            if(valor_total_cesta == 0) {
                Toast.makeText(this, R.string.cesta_vazia, Toast.LENGTH_LONG).show();
                //Fecha a Activity de Cesta
                finish();
            }
            else
                {
                   //Grava a compra e limpa a cesta
                    DataBaseController db = new DataBaseController(getBaseContext());

                    SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                    String strData = fmt.format(new Date());

                    String resultado;

                    //Aqui estou colocando o nome do aparelho usado, mas no caso de um sistema de login
                    //seria um username do usuário.
                    resultado = db.insereCompra(MODEL, strData, valor_total_cesta);

                    Toast.makeText(this, resultado, Toast.LENGTH_LONG).show();

                    //Limpa a cesta
                    Cesta NewCesta = new Cesta();
                    //Persistindo o carro selecionado
                    Util.GravaObjeto(this, "cesta.bin", NewCesta);
                    //Fecha a Activity de Cesta
                    finish();
                }
        }
        catch (Exception e)
        {
            Toast.makeText(this, R.string.Erro_Compra, Toast.LENGTH_LONG).show();
        }
    }
}
