package com.fgomes.desafioandroidbrq.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {

    private static iCarService carService;

    public static final String ENDPOINT_URL = "http://desafiobrq.herokuapp.com/v1/";

    public static iCarService CarService () {
        if (carService == null) {
            Gson gson = new GsonBuilder()
                    .setDateFormat("dd/MM/yyyy'T'HH:mm:ssZ")
                    .create();

            // Add logging into retrofit 2.0
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.interceptors().add(logging);

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(ENDPOINT_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(httpClient.build()).build();

            carService = retrofit.create(iCarService.class);
        }
        return carService;
    }
}