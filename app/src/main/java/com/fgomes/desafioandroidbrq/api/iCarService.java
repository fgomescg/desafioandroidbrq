package com.fgomes.desafioandroidbrq.api;


import com.fgomes.desafioandroidbrq.entity.Car;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Fabio on 17/06/2015.
 */
public interface iCarService {

    @GET("carro/")
    Call<List<Car>> getCars();

    @GET("carro/{id}")
    Call<Car> getCar(@Path("id") int id);


}
