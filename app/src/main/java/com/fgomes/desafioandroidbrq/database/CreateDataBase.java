package com.fgomes.desafioandroidbrq.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class CreateDataBase extends SQLiteOpenHelper {

    public static final String NOME_BANCO = "DB_DESAFIO_BRQ.db";
    public static final String TABELA = "compra";
    public static final String ID = "_id";
    public static final String USUARIO = "usuario";
    public static final String DATA_TRANSACAO = "dt_transacao";
    public static final String VALOR_TRANSACAO = "valor";

    private static final int VERSAO = 1;

    public CreateDataBase(Context context){
        super(context, NOME_BANCO,null,VERSAO);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String sql = "CREATE TABLE "+TABELA +"("
                + ID + " integer primary key autoincrement,"
                + USUARIO + " text,"
                + DATA_TRANSACAO + " text,"
                + VALOR_TRANSACAO + " real "
                +")";

        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS "+TABELA);
        onCreate(db);

    }
}


