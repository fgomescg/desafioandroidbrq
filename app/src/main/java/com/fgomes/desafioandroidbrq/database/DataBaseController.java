package com.fgomes.desafioandroidbrq.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.fgomes.desafioandroidbrq.R;

import java.util.Date;

public class DataBaseController {

    private SQLiteDatabase db;
    private CreateDataBase banco;
    private  Context c;

    public DataBaseController(Context context){
        banco = new CreateDataBase(context);
        c = context;
    }

    public String insereCompra(String usuario, String data, Double valor){
        ContentValues valores;
        long resultado;

        db = banco.getWritableDatabase();
        valores = new ContentValues();
        valores.put(CreateDataBase.USUARIO, usuario);
        valores.put(CreateDataBase.DATA_TRANSACAO, data);
        valores.put(CreateDataBase.VALOR_TRANSACAO, valor);

        resultado = db.insert(CreateDataBase.TABELA, null, valores);
        db.close();

        if (resultado ==-1)
            return c.getString(R.string.Erro_Compra);
        else
            return c.getString(R.string.Sucesso_Compra);

    }
}