package com.fgomes.desafioandroidbrq.interfaces;

import android.view.View;


public interface RecyclerViewOnClickListener {
     void onClickListener(View view, int position);
}
