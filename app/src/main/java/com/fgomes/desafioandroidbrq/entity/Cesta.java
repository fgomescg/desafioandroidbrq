package com.fgomes.desafioandroidbrq.entity;

import java.io.Serializable;
import java.util.List;

public class Cesta implements Serializable {

    private List<Car> carList;

    public List<Car> getcarList() {
        return carList;
    }
    public void setcarList(List<Car> lst) {
        this.carList = lst;
    }

    public double getValorTotalCesta() {

        double valorTotalCesta = 0;
        for (Car car : carList)
            valorTotalCesta += car.getQuantidade() * car.getPreco();

        return valorTotalCesta;
    }


}