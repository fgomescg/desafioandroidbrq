package com.fgomes.desafioandroidbrq.util;


import android.content.Context;
import android.content.SharedPreferences;

import com.fgomes.desafioandroidbrq.activitys.CarDetailActivity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.List;

public class Utility {

    public void GravaDadoInteiro(Context c, String nome, int value )
    {
        //Persistindo o id do carro.
        SharedPreferences sp = c.getSharedPreferences(nome, CarDetailActivity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt(nome, value);
        editor.commit();
    }

    public int LerDadoInteiro(Context c, String nome )
    {
        SharedPreferences sp = c.getSharedPreferences(nome, c.MODE_PRIVATE);
        int myIntValue = sp.getInt(nome, -1);
        return myIntValue;
    }

    public void Delete(Context c, String FileName)
    {
        File file = c.getFileStreamPath(FileName);
        file.delete();

    }
    public void GravaObjeto(Context c, String FileName, Object o) {

        FileOutputStream outputStream;
        ObjectOutputStream ObjOutputStream;

        try {
            outputStream = c.openFileOutput(FileName, Context.MODE_PRIVATE);
            ObjOutputStream = new ObjectOutputStream(outputStream);
            ObjOutputStream.writeObject(o);
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public Object LerObjeto(Context c, String FileName) {

        File file = c.getFileStreamPath(FileName);
        try {
            //Para Ler
            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            Object objRetorno = ois.readObject();
            fis.close();
            ois.close();
            return objRetorno;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
