package com.fgomes.desafioandroidbrq.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fgomes.desafioandroidbrq.R;
import com.fgomes.desafioandroidbrq.activitys.CarDetailActivity_;
import com.fgomes.desafioandroidbrq.activitys.CestaActivity;
import com.fgomes.desafioandroidbrq.activitys.CestaActivity_;
import com.fgomes.desafioandroidbrq.entity.Car;
import com.fgomes.desafioandroidbrq.facade.CestaFacade;
import com.fgomes.desafioandroidbrq.interfaces.RecyclerViewOnClickListener;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;


public class NewAdapter extends RecyclerView.Adapter<NewAdapter.MyViewHolder> {

    private List<Car> mList;
    private LayoutInflater mLayoutInflater;
    private Context mContext;
    private RecyclerViewOnClickListener mRecyclerViewOnClickListener;
    CestaFacade facade = new CestaFacade();

    public NewAdapter (Context c, List<Car> l){
        super();
        this.mContext = c;
        mList = l;
        mLayoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = mLayoutInflater.inflate(R.layout.item_car_card, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
       final Car car = mList.get(position);

        Picasso.with(mContext).load(car.getImagem())
                .placeholder(R.drawable.loagind_image)
                .error(R.drawable.loading_error).into(holder.media_image);

        holder.primary_text.setText(car.getNome());
        NumberFormat nf = new DecimalFormat("R$ #,##0.00", new DecimalFormatSymbols(new Locale("pt", "BR")));
        holder.price.setText(nf.format(car.getPreco()));

        holder.mais_detalhes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Context context = v.getContext();
                Intent intent = new Intent(context, CarDetailActivity_.class);
                intent.putExtra("carroId", car.getId());

                context.startActivity(intent);
            }
        });


        holder.btnComprar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Na Lista Principal só insere 1 qtd.
                car.setQuantidade(1);
                Context context = v.getContext();
                facade.InserirItemCesta(context,car);

            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    public void setRecyclerViewOnClickListener(RecyclerViewOnClickListener r){
        mRecyclerViewOnClickListener = r;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView media_image;
        public TextView primary_text;
        public TextView price;
        public TextView mais_detalhes;
        public Button btnComprar;


        public MyViewHolder(View itemView) {
            super(itemView);

            media_image =  itemView.findViewById(R.id.media_image);
            primary_text = itemView.findViewById(R.id.primary_text);
            price  = itemView.findViewById(R.id.price_text);
            mais_detalhes = itemView.findViewById(R.id.detalhe_text);
            btnComprar = itemView.findViewById(R.id.btComprar);
        }


        @Override
        public void onClick(View view) {
            if(mRecyclerViewOnClickListener != null){
                mRecyclerViewOnClickListener.onClickListener(view, getAdapterPosition());
            }

        }
    }
}
