package com.fgomes.desafioandroidbrq.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fgomes.desafioandroidbrq.R;
import com.fgomes.desafioandroidbrq.activitys.CestaActivity_;
import com.fgomes.desafioandroidbrq.entity.Car;
import com.fgomes.desafioandroidbrq.facade.CestaFacade;
import com.fgomes.desafioandroidbrq.interfaces.RecyclerViewOnClickListener;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;


public class NewAdapterCesta extends RecyclerView.Adapter<NewAdapterCesta.MyViewHolder> {

    private List<Car> mList;
    private LayoutInflater mLayoutInflater;
    private Context mContext;
    CestaFacade facade = new CestaFacade();

    public NewAdapterCesta(Context c, List<Car> l){
        super();
        this.mContext = c;
        mList = l;
        mLayoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = mLayoutInflater.inflate(R.layout.item_cesta, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
       final Car car = mList.get(position);

        Picasso.with(mContext).load(car.getImagem())
                .placeholder(R.drawable.loagind_image)
                .error(R.drawable.loading_error).into(holder.media_image);

        holder.primary_text.setText(car.getNome());
        NumberFormat nf = new DecimalFormat("R$ #,##0.00", new DecimalFormatSymbols(new Locale("pt", "BR")));
        holder.price.setText(nf.format(car.getPreco()));
        holder.txt_Qtd.setText(car.getQuantidade().toString());

        holder.btRemover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Context context = v.getContext();
                facade.RemoverItemCesta(context,car);

                //refresh
                Intent refresh = new Intent(context, CestaActivity_.class);
                context.startActivity(refresh);
                ((Activity)context).finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView media_image;
        public TextView primary_text;
        public TextView price;
        public TextView txtlblQtd;
        public TextView txt_Qtd;
        public Button btRemover;



        public MyViewHolder(View itemView) {
            super(itemView);

            media_image =  itemView.findViewById(R.id.media_image);
            primary_text = itemView.findViewById(R.id.primary_text);
            price  = itemView.findViewById(R.id.price_text);
            txtlblQtd = itemView.findViewById(R.id.lbl_qtd);
            txt_Qtd = itemView.findViewById(R.id.txt_qtd);
            btRemover = itemView.findViewById(R.id.btRemover);
        }

    }
}
